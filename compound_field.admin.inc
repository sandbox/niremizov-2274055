<?php 

/**
 * @file
 * Configuration pages for Compound Field
 */

/**
 *  Settings for for the Compound Field
 */

function compound_field_settings_form_default($field, $instance, $has_data){

  $element = array();
  
  $saved_settings = (array)$field['settings'];
  $posted_settings = isset($_POST['field']['settings']) ? $_POST['field']['settings'] : array();
  $settings = array_merge($saved_settings, $posted_settings);
    
  //List of available field types
  
  $element['fields_number'] = array(
  	'#title' => t('Number of fields'), 
  	'#type' => 'textfield', 
  	'#size' => 15, 
  	'#default_value' => $settings['fields_number'], 
  	'#ajax' => array(
      'callback' => 'compound_field_settings_ajax_callback',
      'wrapper' => 'compound_field-settings-ajax',
    ), 
  	'#required' => TRUE,
  );
  
  // Item for AJAX replacement. - For chooseing field types
  
  // Inner fields configuration box.
  $element['settings'] = array(
	'#type' => 'fieldset',
	'#title' => t('Fields configuration'),
    '#prefix' => '<div id="compound_field-settings-ajax">',
    '#suffix' => '</div>',
  );

  $field_number = $settings['fields_number'];
   
  for ($i=1;$i<=$field_number;$i++){
    
  	$element['settings'][$i] = array (
  	  '#title' => t("Field $i"),
      '#type' => 'fieldset',
  	  //'#prefix' => '<div style="float:left; margin:5px">',
      //'#suffix' => '</div>',  	
  	);
  	
  	$element['settings'][$i]['title'] = array (
  	  '#title' => t("Field $i title"),
      '#type' => 'textfield',
  	  '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['title']) ? 
  				  				$settings['settings'][$i]['title'] : '',
  	  '#size' => 15,

  	);

	// Generating advanced settings for neened field

	$element['settings'][$i]['field_type'] = array (
  	  '#type' => 'select',
	  '#title' => t('Type of field '.$i),
  	  '#required' => TRUE,
      '#options' => compound_field_types(),
  	  '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['field_type']) ? 
  				  				$settings['settings'][$i]['field_type'] : 'none', 
	);
	
	$element['settings'][$i]['link_settings'] = array(
  	  '#type' => 'select',
	  '#title' => t('Take settings from field'),
  	  '#required' => TRUE,
      '#options' => compound_field_link_settings_types(),
  	  '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['link_settings']) ? 
  				  				$settings['settings'][$i]['link_settings'] : 'none',
  	  '#states' => array(
       	'visible' => array(
          	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	'value' => 'node_reference',
       		),
      	),
      ), 
	);
	$element['settings'][$i]['none'] = array(
	  '#type' => 'item',
	  '#title' => t('No settings available for field '.$i),
	  '#states' => array(
       	'visible' => array(
          	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	'value' => 'none',
       		),
      	),
      ),
	);
	$element['settings'][$i]['textfield'] = array(
	  '#type' => 'item',
	  '#title' => t('Text field options for field '.$i),
	  '#states' => array(
       	'visible' => array(
           	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	'value' => 'textfield',
           	),
        ),
      ),
	);
	$element['settings'][$i]['select'] = array(
	  '#type' => 'textarea',
	  '#title' => t('Select field options for field '.$i),
	  '#states' => array(
       	'visible' => array(
           	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	  'value' => 'select',
           	),
        ),
      ),
      '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['select']) ? 
  				  				$settings['settings'][$i]['select'] : NULL, 
	);
	
	$element['settings'][$i]['decimal_separator'] = array(
      '#type' => 'select',
      '#title' => t('Decimal marker'),
      '#options' => array('.' => t('Decimal point'), ',' => t('Comma')),
      '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['decimal_separator']) ? 
  				  				$settings['settings'][$i]['decimal_separator'] : '.', 
	
	
      '#description' => t('The character users will input to mark the decimal point in forms.'),
	  '#states' => array(
       	'visible' => array(
           	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	'value' => 'number_float',
           	),
        ),
      ),
	);
	
	$element['settings'][$i]['d_list'] = array(
	  '#type' => 'select',
	  '#options' => compound_field_vocabularies(),
	  '#title' => t('Text field options for field '.$i),
	  '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['d_list']) ? 
  				  				$settings['settings'][$i]['d_list'] : NULL,
	  '#states' => array(
       	'visible' => array(
           	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	'value' => 'd_list',
           	),
        ),
      ),
	);
	// D_AUTOCOMPLETE - setting int d_autocomplete field is Vocabulary - vid
	$element['settings'][$i]['d_autocomplete'] = array(
	  '#type' => 'select',
	  '#options' => compound_field_vocabularies(),
	  '#title' => t('Text field options for field '.$i),
	  '#default_value' => isset($settings['settings'][$i]) && isset($settings['settings'][$i]['d_autocomplete']) ? 
  				  				$settings['settings'][$i]['d_autocomplete'] : NULL,
	  '#states' => array(
       	'visible' => array(
           	':input[name="field[settings][settings]['. $i .'][field_type]"]' => array(
           	'value' => 'd_autocomplete',
           	),
        ),
      ),
	);

	
  }
  return $element;
}

/**
 * Callback element needs only select the portion of the form to be updated.
 * Since #ajax['callback'] return can be HTML or a renderable array (or an
 * array of commands), we can just return a piece of the form.
 */
function compound_field_settings_ajax_callback($form, $form_state) {
	/* @TODO Create 'options' for each field, for not saveing extra data.
	 */
  return $form['field']['settings']['settings'];
}

/**
 * 
 * Function that return list of node reference fields
 */

function compound_field_link_settings_types(){

	$list = array('none' => 'none');
	$data = field_read_fields(array('type' => 'node_reference'));

	foreach ($data as $field){
		$instances = field_read_instances(array('field_name' => $field['field_name'])); 
		foreach ($instances as $instance){
			$list += array (
				$instance['entity_type'].'/'.$instance['bundle'].'/'.$field['field_name'] => $instance['label'].' ['.$field['field_name'].' - '.$instance['bundle'].']',
			);
		}
	}
	
	return $list;
}

/**
 * 
 * Function that return list of vocabulary in normal array form
 */

function compound_field_vocabularies(){

  $vocabularies = taxonomy_get_vocabularies();
  //kpr($vocabularies);
  $options = array();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->vid] = $vocabulary->name;
  }
  return $options;
}