/*
 * @file
 * Adds behaviours nessesary for triggering pasting tabular data from Excel
 * to input table and inserting this data in each input cell.
 */

(function($) {
	/**
	 * Add behaviour that add to the ancet dinamic status calculation.
	 */
	Drupal.behaviors.compound_field_table = {
		attach: function(context, settings) {
			$(".field-type-compound .form-tablefield input", context).bind(
					'paste',
					function(e) {
						var el = $(this);
						// Find element delta
						matches = this.id.match(/tablefield_(\d)*_cell/);
						delta = matches[1];
						// Set timeout, so we will access new passted values
						setTimeout(function() {
							var data = $(el).val();
							// After retrieving data, parce it by each cell.
							// Columns delimiter is TAB, rows delimiter is /r/n.
							// But after data is passed
							// to input field string is strippid from /r/n to
							// just /r(or /n) character.
							// So it is quite difficult to split cell right,
							// neccesary condtition is "no space"
							// inside cell data.. input todo here
							// @TODO Create better way of splitting rows, so
							// cell would include spaces.
							var cells = data.split(/[ ]|[\t]/);
							var column, row, result;
							for ( var y in cells) {
								row = Math.floor(y / 7);
								column = y - (row * 7);
								result = cells[(7 * row) + column];
								$(
										'#tablefield_' + delta + '_cell_' + row
												+ '_' + column).val(result);
							}
						}, 50);
					});
		}
	};
})(jQuery);
