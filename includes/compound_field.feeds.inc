<?php


/*
function compound_field_node_validate($node, $form, &$form_state) {
	//$term = compound_field_term_lookup_term('Санкт-Петербург', '2');
	//kpr(field_info_field('field_city_soiskatel'));
	//kpr($node);
	//form_set_error('okey_rules', t('You cannot edit material in such way, becouse material with same fields already exists.'));
}
*/

/**
* Callback for mapping. Here is where the actual mapping happens.
*
* When the callback is invoked, $target contains the name of the field the
* user has decided to map to and $value contains the value of the feed item
* element the user has picked as a source.
*/
function compound_field_feeds_set_target($source, $entity, $target, $value) {
	if (empty($value)) {
		return;
	}

	// Handle non-multiple value fields.
	//if (!is_array($value)) {
	//	$value = array($value);
	//}

	// Iterate over all values.
	$i = 0;
	list($field_name, $sub_field) = explode('|', $target);
	list($field_number, $field_type) = explode ("_", $sub_field, 2);
	//$info = field_info_field($field_name);

	switch ($field_type) {
		case "d_autocomplete":
			$field['und']['0'][$sub_field] = compound_field_taxonomy_target($value, $field_name, $field_number, $field_type);
			break;
	}
	//$field['und']['0']['1_d_autocomplete'] = compound_field_taxonomy_target($value, $field_name, $field_number, $field_type);;
	//$field['und']['0']['2_textfield'] = '1';
	//$field['und']['0']['3_d_list'] = '1';
	$entity->{$field_name} = $field;
}

function compound_field_taxonomy_target($value, $field_name, $field_number, $field_type) {
	$info = field_info_field($field_name);

	// See http://drupal.org/node/881530
	if (isset($info['settings']['settings'][$field_number][$field_type])) {
		$vid = $info['settings']['settings'][$field_number][$field_type];
	}

	//foreach ($value as $name){
	//$field['und']['0'][$sub_field] = $v;
	//dpr($name);
	$term = compound_field_term_lookup_term($value, $vid);
	/*if (empty($term)) {
	 $term = new stdClass();
	$term->name = $name;
	$term->vid = $vid;
	taxonomy_term_save($term);
	return $term->tid;
	}*/
	$field = $term->tid;
	//}
	return $field;
}

/**
* Looks up a term, assumes SQL storage backend.
*/

function compound_field_term_lookup_term($name, $vid) {
	return db_select('taxonomy_term_data', 'td')
	->fields('td', array('tid', 'name'))
	->condition('name', $name)
	->condition('vid', $vid)
	->execute()
	->fetchObject();
}
